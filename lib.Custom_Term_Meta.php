<?php

/* 
Name: Custom Term Meta Class 
Filename: Custom_Term_Meta.php
Author: Chester Alan Tagudin
Author URL: http://www.chesteralan.com/
*/

if (! class_exists( 'Custom_Term_Meta' )) {

class Custom_Term_Meta {
   
   var $fields = array();
   var $taxonomy = 'post_tag';
   
   function __construct($tax='post_tag') {
        $this->taxonomy = $tax;
   }
   
   function init() {
        add_action ( 'edit_tag_form_fields', array(&$this, 'metabox') );
        add_action ( 'edited_terms', array(&$this, 'save' ) );
		add_action("admin_print_scripts", array(&$this, 'page_scripts'));
		add_action("admin_print_styles", array(&$this,'page_styles'));

    }

 
	function page_styles() {
		wp_enqueue_style('thickbox');
	}
	  
	function page_scripts() {
		wp_enqueue_script('thickbox');
	}
	
    function metabox($tag) {
        if( $tag->taxonomy == $this->taxonomy ) {
           foreach($this->fields as $field) {
            $this->show_field($field, $tag->term_id);
           }
        }
    }
     
    function save($term_id) {
            if( isset($_POST['taxonomy']) && ($_POST['taxonomy'] == $this->taxonomy) ) {
                foreach($this->fields as $field) {
                    if ( isset( $_POST[$field['meta_key']] ) && ( $_POST[$field['meta_key']] != '') ) {
                        update_custom_termmeta($term_id, $field['meta_key'], esc_attr($_POST[$field['meta_key']]) );
                    }
                }
            }
    }
    
    function add_field(Array $field) {
        $defaults = array('label' => 'New Input', 'meta_key'=> '', 'meta_value'=> '', 'desc'=>'', 'type' => 'text');
        $this->fields[] = array_merge($defaults, $field);
        return $this;
    }
    
    function set_taxonomy($tax) {
        $this->taxonomy = $tax;
        return $this;
    }
    
    function show_field(Array $field, $id) {
            $current_value = get_custom_termmeta($id, $field['meta_key'], TRUE);
            
            if( $current_value == '' ) {
                $current_value = $field['meta_value'];
            }
            
            echo '<tr class="form-field">
                        <th scope="row" valign="top"><label for="'.$field['meta_key'].'">'.$field['label'].'</label></th>
                        <td>';
            switch($field['type']) {
					// text
					case 'text':
						echo '<input type="text" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// text
					case 'text_media':
$image_library_url = get_upload_iframe_src( 'image', null, 'library' );
$image_library_url = remove_query_arg( array('TB_iframe'), $image_library_url );
$image_library_url = add_query_arg( array( 'context' => 'shiba-gallery-default-image', 'TB_iframe' => 1 ), $image_library_url );

						echo '<input type="text" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$current_value.'" size="30" />
							<a href="'. esc_url( $image_library_url ) .'" id="'.$field['meta_key'].'_button" class="button thickbox">Open Media</a>
								<br /><span class="description">'.$field['desc'].'</span>';



echo <<<HTML
<script type="text/javascript">
    var win = window.dialogArguments || opener || parent || top; 
	win.send_to_editor = function( html ) {
		var patt = /src=[\"'](.*)[\"'] alt=/ig;
		var res = patt.exec( html );
		if(typeof res[1] !== 'undefined'){
			win.jQuery('#{$field['meta_key']}').val( res[1] );
		}
		win.tb_remove();
	}
    </script>
HTML;
								
					break;
					// textarea
					case 'textarea':
						echo '<textarea name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" cols="60" rows="4">'.$current_value.'</textarea>
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox
					case 'checkbox':
						echo '<input type="checkbox" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" ',$current_value ? ' checked="checked"' : '','/>
								<label for="'.$field['meta_key'].'">'.$field['desc'].'</label>';
					break;
					// select
					case 'select':
						echo '<select name="'.$field['meta_key'].'" id="'.$field['meta_key'].'">';

						foreach ($field['options'] as $option) {
							echo '<option', $current_value == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
						}
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// radio
					case 'radio':
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="'.$field['meta_key'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$current_value == $option['value'] ? ' checked="checked"' : '',' />
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox_group
					case 'checkbox_group':
						foreach ($field['options'] as $option) {
							echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['meta_key'].'[]" id="'.$option['value'].'"',$current_value && in_array($option['value'], $current_value) ? ' checked="checked"' : '',' /> 
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// tax_select
					case 'tax_select':
						echo '<select name="'.$field['meta_key'].'" id="'.$field['meta_key'].'">
								<option value="">Select One</option>'; // Select One
						$terms = get_terms($field['meta_key'], 'get=all');
						$selected = wp_get_object_terms($post->ID, $field['meta_key']);
						foreach ($terms as $term) {
							if (!empty($selected) && !strcmp($term->slug, $selected[0]->slug)) 
								echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>'; 
							else
								echo '<option value="'.$term->slug.'">'.$term->name.'</option>'; 
						}
						$taxonomy = get_taxonomy($field['meta_key']);
						echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['meta_key'].'">Manage '.$taxonomy->label.'</a></span>';
					break;
					// post_list
					case 'post_list':
					$items = get_posts( array (
						'post_type'	=> $field['post_type'],
						'posts_per_page' => -1
					));
						echo '<select name="'.$field['meta_key'].'" id="'.$field['meta_key'].'">
								<option value="">Select One</option>'; // Select One
							foreach($items as $item) {
								echo '<option value="'.$item->ID.'"',$current_value == $item->ID ? ' selected="selected"' : '','>'.$item->post_type.': '.$item->post_title.'</option>';
							} // end foreach
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'date':
						echo '<input type="text" class="datepicker" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'datetime':
						echo '<input type="text" class="datetimepicker" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// slider
					case 'slider':
					$value = $current_value != '' ? $current_value : '0';
						echo '<div id="'.$field['meta_key'].'-slider"></div>
								<input type="text" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$value.'" size="5" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// image
					case 'image':
						$image = get_template_directory_uri().'/images/image.png';	
						echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
						if ($current_value) { $image = wp_get_attachment_image_src($current_value, 'medium');	$image = $image[0]; }				
						echo	'<input name="'.$field['meta_key'].'" type="hidden" class="custom_upload_image" value="'.$current_value.'" />
									<img src="'.$image.'" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small>&nbsp;<a href="#" class="custom_clear_image_button">Remove Image</a></small>
										<br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
					// repeatable
					case 'repeatable':
						echo '<a class="repeatable-add button" href="#">+</a>
								<ul id="'.$field['meta_key'].'-repeatable" class="custom_repeatable">';
						$i = 0;
						if ($current_value) {
							foreach($current_value as $row) {
								echo '<li><span class="sort hndle">|||</span>
											<input type="text" name="'.$field['meta_key'].'['.$i.']" id="'.$field['meta_key'].'" value="'.$row.'" size="30" />
											<a class="repeatable-remove button" href="#">-</a></li>';
								$i++;
							}
						} else {
							echo '<li><span class="sort hndle">|||</span>
										<input type="text" name="'.$field['meta_key'].'['.$i.']" id="'.$field['meta_key'].'" value="" size="30" />
										<a class="repeatable-remove button" href="#">-</a></li>';
						}
						echo '</ul>
							<span class="description">'.$field['desc'].'</span>';
					break;
				} //end switch
				echo '</td>
    </tr>';
        }
}

}

function Custom_Term_Meta_Activation()
{
    global $wpdb;
    
    $termmeta_table = $wpdb->prefix . "termmeta";
    $termmeta_sql = "CREATE TABLE `$termmeta_table` (
         `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
         `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
         `meta_key` varchar(255) DEFAULT NULL,
         `meta_value` longtext,
         PRIMARY KEY (`meta_id`),
         KEY `term_id` (`term_id`),
         KEY `meta_key` (`meta_key`)
        );";
        
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $termmeta_sql );
	
}

register_activation_hook( __FILE__, 'Custom_Term_Meta_Activation' );

if( ! function_exists( 'add_custom_termmeta' ) ) {
    function add_custom_termmeta($term_id, $meta_key, $meta_value) {
        global $wpdb;
        $wpdb->insert( 
	        $wpdb->prefix . "termmeta", 
	        array( 
		        'term_id' => $term_id, 
		        'meta_key' => $meta_key ,
		        'meta_value' => $meta_value
	        )
        );
    }
}

if( ! function_exists( 'update_custom_termmeta' ) ) {
    function update_custom_termmeta($term_id, $meta_key, $meta_value) {
        global $wpdb;
        $rows = $wpdb->update( 
	        $wpdb->prefix . "termmeta", 
	        array( 
		        'meta_value' => $meta_value
	        ),
	        array( 
		        'term_id' => $term_id, 
		        'meta_key' => $meta_key		        
	        )
        );

        if( ( $rows === false ) || ( $rows == 0 ) ) {
             add_custom_termmeta($term_id, $meta_key, $meta_value); 
        }
    }
}

if( ! function_exists( 'get_custom_termmeta' ) ) {
    function get_custom_termmeta($term_id, $meta_key, $return_value=false) {
        global $wpdb;
        $result = $wpdb->get_row( $wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "termmeta WHERE term_id = %s AND meta_key = '%s';", $term_id, $meta_key ) );
        if($return_value) {
            return $result->meta_value;
        } else {
            return $result;
        }
    }
}
