<?php

if (! class_exists( 'Custom_Post_Type' )) {

class Custom_Post_Type 
    {
        var $id;
        var $name;
        var $plural;
        var $description;
        var $public = true;
        var $has_archive = true;
        var $capability_type = 'page';
        var $supports = array( 'title' ); // 'editor' , 'excerpt', 'thumbnail',  'comments', 'page-attributes' ); 'custom-fields', 'author', 'trackbacks', 'post-formats');
        var $position = NULL;
		var $hierarchical = false;
		var $labels = array();
        
        function __construct($id='', $name='', $plural='', $description = 'Description of Custom Post Type Here', $position=NULL) {
            $this->id = $id;
            $this->name = $name;
            $this->plural = $plural;
            $this->description = $description;
            $this->position = $position;
        }    
        
        function init()
        {
            if( $this->id && $this->name ) {
                add_action( 'init', array(&$this, 'register_post_type' ) );
            }
        }
		
        function register_post_type()
        {
	         $args = array(
		        'labels' => $this->get_labels(),
		        'description' => __( $this->description ),
		        'public' => $this->public,
		        'has_archive' => $this->has_archive,
		        'rewrite' => array('slug' => $this->id),
		        'menu_position' => $this->position,
		        'menu_icon' =>  plugins_url( 'images/' . $this->id . '-icon.png', dirname(__FILE__)  ),
		        'capability_type'=>$this->capability_type,
		        'supports'=>$this->supports,
				'hierarchical'=>$this->hierarchical,
		        );
		        
	        register_post_type( $this->id , $args);
	        
        }
        
		function get_labels() {
			$defaults = array(
				    'name' => __( $this->plural ),
				    'singular_name' => __( $this->name ),
				    'menu_name' => __( $this->plural ),
				    'all_items' => __('All ' . $this->plural ),
				    'add_new' => __('Add a ' . $this->name),
				    'add_new_item' => __('Add a ' . $this->name),
				    'edit_item' => __('Edit ' . $this->name),
				    'new_item' => __('New ' . $this->name),
				    'view_item' => __('View ' . $this->name),
				    'search_items' => __('Search ' . $this->plural),
				    'not_found' => __('No ' . $this->name . ' found'),
				    'not_found_in_trash' => __('No ' . $this->name . ' found in Trash'),
				    'parent_item_colon' => __('Parent ' . $this->name),
			    );
			
			return array_merge($defaults, $this->labels);
		}
		
		function set_label( $key, $value ) {
				$this->labels = array_merge($this->labels, array($key => $value));
				return $this;
		}
		
        
       function set_id($value) {
            $this->id = $value;
            return $this;
       }
       
       function set_name($value) {
            $this->name = $value;
            return $this;
       }
       
       function set_plural($value) {
            $this->plural = $value;
            return $this;
       }
       
       function set_description($value) {
            $this->description = $value;
            return $this;
       }  
       
       function set_public($value) {
            $this->description = (bool) $value;
            return $this;
       }  
       
       function set_has_archive($value) {
            $this->description = (bool) $value;
            return $this;
       }    
       
       function set_position($value) {
            $this->position = (int) $value;
            return $this;
       }   
       
       function set_capability_type($value) {
            $this->capability_type = $value;
            return $this;
       }   
       
       function set_supports($value) {
            $this->supports = $value;
            return $this;
       }
       
	   function set_hierarchical($value=false) {
		   $this->hierarchical = $value;
		   return $this;
	   }
	   
       function add_support($value) {
            $numargs = func_num_args();
            if ($numargs >= 2) {
                for ($i = 0; $i < $numargs; $i++) {
                    $this->supports[] = func_get_arg($i);
                }
            } else {
                $this->supports[] = $value;
            }
            return $this;
       }    
    }
}
