<?php
/* 
Name: Custom Post Type Class 
Filename: Custom_Post_Type.php
Author: Chester Alan Tagudin
Author URL: http://www.chesteralan.com/
*/

if (! class_exists( 'Custom_Metabox' )) {
    class Custom_Metabox
    {
        var $id;
        var $title;
        var $post_type;
        var $context; // ('normal', 'advanced', or 'side')
        var $priority; // ('high', 'core', 'default' or 'low')
        var $fields = array();
        var $tabs = array();
        
        function __construct($post_type, $id, $title, $context, $priority) {
            $this->id = $id;
            $this->title = $title;
            $this->post_type = $post_type;
            $this->context = $context;
            $this->priority = $priority;
            return $this;
        }    
        
        function init()
        {
            add_action('add_meta_boxes', array(&$this,'add_meta_box'));
            add_action('save_post', array(&$this, 'save_post'));
        }
        
        function add_meta_box() {
                add_meta_box($this->id, $this->title, array(&$this, 'meta_box_content'), $this->post_type, $this->context, $this->priority);
        }
        
        function meta_box_content()
        {
            global $post;
            echo '<input type="hidden" name="custom_meta_box_nonce_'.$post->ID.'" value="'.wp_create_nonce( 'custom_meta_box_nonce_'. $post->ID ).'" />';
            
            if( count($this->tabs) > 0 ) {
                echo '<div class="metabox-tabs"><ul>';
                    foreach($this->tabs as $tab) {
                        echo '<li '.$default.'><a href="#'.$tab['id'].'">'.$tab['label'].'</a></li>';
                    }
                echo '</ul>';
                
                foreach($this->tabs as $tab) {
                    echo '<div id="'.$tab['id'].'">';
                        if( count($this->fields) > 0) {
                            foreach($this->fields as $field) {
                                if( isset( $field['tab'] ) && $field['tab'] == $tab['id'] ) {
                                    $this->show_field($field, $post->ID);
                                }
                            }
                        }
                    echo '</div>';                    
                }
                
                echo '</div>';
            } else {
                if( count($this->fields) > 0) {
                    foreach($this->fields as $field) {
                        $this->show_field($field, $post->ID);
                    }
                }
            }
       
        }
        
        function add_tab( Array $tab ) {
        
            $defaults = array(
                'id' => '',
                'label' => ''
            );
            
            $this->tabs[] = array_merge($defaults, $tab);
        }
        
        function add_field(Array $field) {
            $defaults = array(
                'id' => '',
                'label' => '',
                'desc' => '',
                'options' => array(),
                'default' => '',
				'styles'=>'',
				'placeholder'=>''
            );
            $this->fields[] =  array_merge($defaults, $field );
        }
        
        function show_field(Array $field, $id) {
            $current_value = get_post_meta($id, $field['id'], true);
            switch($field['type']) {                    
					// text
					case 'text':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="30" style="'.$field['styles'].'" placeholder="'.$field['placeholder'].'" />
								<span class="description">'.$field['desc'].'</span>';
					break;
					// textarea
					case 'textarea':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$current_value.'</textarea>
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// select
					case 'select':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';

						foreach ($field['options'] as $option) {
							echo '<option', $current_value == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
						}
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox
					case 'checkbox':
						echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ', $current_value ? ' checked="checked"' : '','/>
								<label for="'.$field['id'].'">'.$field['label'].'</label>';
						echo '<p><em><small class="description">'.$field['desc'].'</small></em></p>';
					break;
					// radio
					case 'radio':
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$current_value == $option['value'] ? ' checked="checked"' : '',' />
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox_group
					case 'checkbox_group':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						foreach ($field['options'] as $option) {
							echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['id'].'[]" id="'.$option['value'].'"',$current_value && in_array($option['value'], $current_value) ? ' checked="checked"' : '',' /> 
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// tax_select
					case 'tax_select':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
						$terms = get_terms($field['id'], 'get=all');
						$selected = wp_get_object_terms($post->ID, $field['id']);
						foreach ($terms as $term) {
							if (!empty($selected) && !strcmp($term->slug, $selected[0]->slug)) 
								echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>'; 
							else
								echo '<option value="'.$term->slug.'">'.$term->name.'</option>'; 
						}
						$taxonomy = get_taxonomy($field['id']);
						echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">Manage '.$taxonomy->label.'</a></span>';
					break;
					// post_list
					case 'post_list':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
					    $items = get_posts( array (
						    'post_type'	=> $field['post_type'],
						    'posts_per_page' => -1
					    ));
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
							foreach($items as $item) {
								echo '<option value="'.$item->ID.'"',$current_value == $item->ID ? ' selected="selected"' : '','>'.$item->post_type.': '.$item->post_title.'</option>';
							} // end foreach
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'date':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<input type="text" class="datepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'datetime':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<input type="text" class="datetimepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// slider
					case 'slider':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						$value = $current_value != '' ? $current_value : '0';
						echo '<div id="'.$field['id'].'-slider"></div>
								<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$value.'" size="5" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// image
					case 'image':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						$image = get_template_directory_uri().'/images/image.png';	
						echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
						if ($current_value) { $image = wp_get_attachment_image_src($current_value, 'medium');	$image = $image[0]; }				
						echo	'<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$current_value.'" />
									<img src="'.$image.'" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small>&nbsp;<a href="#" class="custom_clear_image_button">Remove Image</a></small>
										<br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
					// repeatable
					case 'repeatable':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<a class="repeatable-add button" href="#">+</a>
								<ul id="'.$field['id'].'-repeatable" class="custom_repeatable">';
						$i = 0;
						if ($current_value) {
							foreach($current_value as $row) {
								echo '<li><span class="sort hndle">|||</span>
											<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="'.$row.'" size="30" />
											<a class="repeatable-remove button" href="#">-</a></li>';
								$i++;
							}
						} else {
							echo '<li><span class="sort hndle">|||</span>
										<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="" size="30" />
										<a class="repeatable-remove button" href="#">-</a></li>';
						}
						echo '</ul>
							<span class="description">'.$field['desc'].'</span>';
					break;
					
				} //end switch
				echo '</p>';
        }
        
        function save_post( $post_id ) {
	        global $post;

	        // verify nonce
	        if ( ! wp_verify_nonce( $_POST[ 'custom_meta_box_nonce_'. $post_id ], 'custom_meta_box_nonce_'. $post_id ) ) 
		        return $post_id;
		 
	        // check autosave
	        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		        return $post_id;
		
	        // check permissions
	        if ('page' == $_POST['post_type']) {
		        if (!current_user_can('edit_page', $post_id)) {
			        return $post_id;
		        } elseif (!current_user_can('edit_post', $post_id)) {
			        return $post_id; 
			        }
	        }
	
	

		        if( $this->post_type == $post->post_type ) {
			

			        foreach ($this->fields as $field) {
				        if($field['type'] == 'tax_select') continue;

				        $old = get_post_meta($post_id, $field['id'], true);
				
				        $new = $_POST[$field['id']];

				        if ($new && $new != $old) {
					        update_post_meta($post_id, $field['id'], $new);
				        } elseif ('' == $new && $old) {
					        delete_post_meta($post_id, $field['id'], $old);
				        }
			        }
		        }	

        }        
        

    }
}
